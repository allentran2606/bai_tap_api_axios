const BASE_URL = "https://635f4b20ca0fe3c21a991e72.mockapi.io";

let idEdited = null;

// Render todo
function fetchAllToDo() {
  onLoading();
  axios({
    url: `${BASE_URL}/baitapapi_axios`,
    method: "GET",
  })
    .then(function (res) {
      offLoading();
      renderList(res.data);
    })
    .catch(function (error) {
      offLoading();
      console.log(`error: `, error);
    });
}
fetchAllToDo();

// Remove todo
function removeTodo(id) {
  onLoading();
  axios({
    url: `${BASE_URL}/baitapapi_axios/${id}`,
    method: "DELETE",
  })
    .then(function (res) {
      offLoading();
      fetchAllToDo();
    })
    .catch(function (error) {
      offLoading();
      console.log(`error: `, error);
    });
}

// Add todo
function addTodo() {
  var data = layThongTinTuForm();
  let newTodo = {
    name: data.name,
    desc: data.desc,
    isComplete: true,
  };
  axios({
    url: `${BASE_URL}/baitapapi_axios`,
    method: "POST",
    data: newTodo,
  })
    .then(function (res) {
      offLoading();
      fetchAllToDo();
    })
    .catch(function (error) {
      offLoading();
      console.log(`error: `, error);
    });
}

function editTodo(id) {
  onLoading();
  axios({
    url: `${BASE_URL}/baitapapi_axios/${id}`,
    method: "GET",
  })
    .then(function (res) {
      offLoading();
      document.getElementById(`name`).value = res.data.name;
      document.getElementById(`desc`).value = res.data.desc;
      idEdited = res.data.id;
    })
    .catch(function (error) {
      turnOffLoading();
      console.log(`error: `, error);
    });
}

function updateTodo() {
  var data = layThongTinTuForm();
  var newTodo = {
    name: data.name,
    desc: data.desc,
    isComplete: true,
  };
  axios({
    url: `${BASE_URL}/baitapapi_axios/${idEdited}`,
    method: "PUT",
    data: data,
  })
    .then(function (res) {
      fetchAllToDo();
    })
    .catch(function (error) {
      console.log(`error: `, error);
    });
}
